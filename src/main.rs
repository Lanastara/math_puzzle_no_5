use std::collections::{HashMap, BTreeMap};

#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord)]
struct Move(u8, u8, u8);

impl Move {
    fn start(&self) -> u8 {
        self.0
    }

    fn jump(&self) -> u8 {
        self.1
    }

    fn end(&self) -> u8 {
        self.2
    }

    fn is_valid(&self, state: u16) -> bool {
        get_position(state, self.jump()) && !get_position(state, self.end())
    }

    fn apply(&self, state: u16) -> u16 {
        ((state ^ (1 << self.0)) ^ (1 << self.1) ^ (1 << self.2))
    }
}

fn triangle_number(x: u8) -> u8 {
    (x * (x + 1)) / 2
}

fn triangle_row(number: u8, triangle_numbers: &Vec<u8>) -> u8 {
    for (index, t) in triangle_numbers.iter().enumerate() {
        if *t > number {
            return index as u8;
        }
    }

    panic!("invalid triangle row");
}

fn build_theoretical_move_list(rows: u8) -> HashMap<u8, Vec<Move>> {
    let mut triangle_numbers = Vec::with_capacity(rows.into());

    for i in 1..rows + 1 {
        triangle_numbers.push(triangle_number(i));
    }

    let max_position = triangle_number(rows);

    let mut ret = HashMap::with_capacity(max_position.into());

    for position in 0..max_position {
        let mut moves = Vec::new();

        let row = triangle_row(position, &triangle_numbers);

        if row > 1 {
            let prev_row = position - (row);
            let prev_prev_row = prev_row - (row - 1);

            if triangle_row(prev_prev_row, &triangle_numbers) == row - 2 {
                moves.push(Move(position, prev_row, prev_prev_row))
            }

            if prev_prev_row > 1 && triangle_row(prev_prev_row - 2, &triangle_numbers) == row - 2 {
                moves.push(Move(position, prev_row - 1, prev_prev_row - 2))
            }
        }
        if row < rows - 2 {
            let next_row = position + row + 1;
            let next_next_row = next_row + row + 2;

            moves.push(Move(position, next_row, next_next_row));
            moves.push(Move(position, next_row + 1, next_next_row + 2));
        }

        if position > 1 && triangle_row(position - 2, &triangle_numbers) == row {
            moves.push(Move(position, position - 1, position - 2));
        }

        if position < max_position - 2 && triangle_row(position + 2, &triangle_numbers) == row {
            moves.push(Move(position, position + 1, position + 2));
        }

        ret.insert(position, moves);
    }

    ret
}

fn get_position(board_state: u16, position: u8) -> bool {
    ((board_state >> position) & 1) == 1
}

fn possible_ends(rows: u8) -> Vec<u16> {
    let triangle = triangle_number(rows);
    let mut ret = Vec::with_capacity(triangle.into());
    for i in 0..triangle {
        ret.push(1u16 << i);
    }

    ret
}

fn possible_starts(rows: u8) -> Vec<u16> {
    let triangle = triangle_number(rows);
    let mut ret: Vec<u16> = possible_ends(rows)
        .iter()
        .map(|state| state ^ ((1 << triangle) - 1))
        .collect();
    ret.sort();
    ret
}

fn paths(start: &u16, ends: &Vec<u16>, states: &HashMap<u16, Vec<Move>>) -> Option<Vec<Vec<Move>>> {
    if let Ok(_) = ends.binary_search(start) {
        return Some(vec![Vec::new()]);
    }

    if let Some(valid_moves) = states.get(start) {
        let mut rets = Vec::new();

        for valid_move in valid_moves {
            let new_start = valid_move.apply(*start);

            if let Some(moves) = paths(&new_start, ends, states) {
                let mut moves = moves
                    .iter()
                    .map(|list| {
                        let mut ret = vec![valid_move.clone()];
                        for i in list {
                            ret.push(i.clone())
                        }
                        ret
                    })
                    .collect();
                rets.append(&mut moves);
            }
        }
        return Some(rets);
    }

    None
}

fn get_length(path: &Vec<Move>) -> u8
{
    let mut len = 1;
    for window in path.windows(2)
    {
        if window[0].end() != window[1].start(){
            len +=1;
        }
    }

    len
}

fn main() {
    let start_time = std::time::Instant::now();

    let rows = 4;
    let triangle = triangle_number(rows);

    let theoretical_move_list = build_theoretical_move_list(rows);

    let mut states = HashMap::with_capacity(1024);

    for i in 0..2u16.pow(triangle.into()) {
        let mut moves = Vec::new();
        for index in 0..triangle {
            let position = get_position(i, index);

            if position {
                let plausible_moves = theoretical_move_list.get(&index);

                if let Some(plausible_moves) = plausible_moves {
                    for plausible_move in plausible_moves {
                        if plausible_move.is_valid(i) {
                            moves.push(plausible_move.clone());
                        }
                    }
                }
            }
        }

        states.insert(i, moves);
    }

    let possible_ends = possible_ends(rows);
    let possible_starts = possible_starts(rows);

    // dbg!(&possible_ends);
    // dbg!(&possible_starts);

    // let path = paths(&389, &possible_ends, &states);

    // dbg!(path);

    let mut paths_by_length : BTreeMap<u8, Vec<(u16, Vec<Move>)>> = BTreeMap::new();

    let mut min_path_length = std::u8::MAX;

    for start in possible_starts
    {
        let paths = paths(
            &start,
            &possible_ends,
            &states
        );

        if let Some(ref paths) = paths{
            for path in paths{
                let length = get_length(&path);
                if length < min_path_length
                {
                    min_path_length = length;
                }
                if let Some(paths) = paths_by_length.get_mut(&length)
                {
                    paths.push((start, path.clone()));
                } else {
                    paths_by_length.insert(length, vec![(start, path.clone())]);
                }
                
            }
        }

    }
    
    let shortest_paths = paths_by_length.get(&min_path_length);

    if let Some(shortest_paths) = shortest_paths
    {
        let end_time = std::time::Instant::now();
        println!("Calculation took: {:?}", end_time-start_time);
        println!("There are {} shortest Paths with a length of {}. \n", shortest_paths.len(), min_path_length);

        for (start, path) in shortest_paths
        {
            
            let mut prev_end = std::u8::MAX;

            let mut state = *start;
            
            for curr_move in path{
                if curr_move.start() != prev_end
                {
                    print!(" {}", curr_move.start()+1);
                }

                print!("-{}", curr_move.end()+1);

                prev_end= curr_move.end();

                state = curr_move.apply(state);
            }

            println!();
        }

    }
}
